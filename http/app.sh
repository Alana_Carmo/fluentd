#!/bin/sh
while true
do
  curl -X POST -d 'json={"status":"success", "message":"message success"}' http://fluentd_1:9880/http.log
	sleep 1

  curl -X POST -d 'json={"status":"warning", "message":"message warning"}' http://fluentd_1:9880/http.log
	sleep 1

  curl -X POST -d 'json={"status":"error", "message":"message error"}' http://fluentd_1:9880/http.log
	sleep 1

done
